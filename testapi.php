<?php
$testData = array(
    "REQUEST_METHOD" => $_SERVER["REQUEST_METHOD"], // lấy request method
    "REQUEST_BODY" => file_get_contents('php://input'), // lấy dữ liệu gửi qua body
    "\$_GET" => $_GET,
    "header" => getallheaders()
);
apiOutput(json_encode($testData));
// Hàm trả kết quả về cho client
function apiOutput($jsonString, $responseCode = 200){
    header('Content-Type: application/json'); // thiết lập content type là application/json
    http_response_code($responseCode); // Thiết lập mã trạng thái trả về
    echo $jsonString;   // trả về chuỗi JSON
    die();//Kết thúc chương trình
}
