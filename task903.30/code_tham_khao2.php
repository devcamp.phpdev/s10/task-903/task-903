<?php
$books = json_decode(file_get_contents($books_file), true);

switch ($method) {
    case 'GET':
        if (isset($request[0])) {
            $id = intval($request[0]);
            $book = array_filter($books, function($b) use ($id) { return $b['id'] == $id; });
            $book = array_values($book);
            echo json_encode($book[0]);
        } else {
            echo json_encode($books);
        }
        break;
    case 'POST':
        $new_id = end($books)['id'] + 1;
        $input['id'] = $new_id;
        $books[] = $input;
        file_put_contents($books_file, json_encode($books));
        echo json_encode($input);
        break;
    case 'PUT':
        $id = intval($request[0]);
        $updated_book = null;
        foreach ($books as &$book) {
            if ($book['id'] == $id) {
                $book = array_merge($book, $input);
                $updated_book = $book;
                break;
            }
        }
        file_put_contents($books_file, json_encode($books));
        echo json_encode($updated_book);
        break;
    case 'DELETE':
        $id = intval($request[0]);
        $books = array_filter($books, function($b) use ($id) { return $b['id'] != $id; });
        file_put_contents($books_file, json_encode(array_values($books)));
        echo json_encode(["deleted" => $id]);
        break;
    default:
        http_response_code(405);
        break;
}
?>