<?php
    class users {
        public $users;
        public function __construct() {
            // Lấy nội dung chuỗi json từ file users.json
            $fileContent = file_get_contents("users.json");
            // Chuyển dữ liệu về dạng mảng
            $this->users = json_decode($fileContent, true);
        }
        public function apiOutput($jsonString, $responseCode = 200){
            header('Content-Type: application/json'); // thiết lập content type là application/json
            http_response_code($responseCode); // Thiết lập mã trạng thái trả về
            echo $jsonString;   // trả về chuỗi JSON
            die();//Kết thúc chương trình
        }
        //Phương thức getAllUsers(): Gọi phương thức apiOutput() trả về toàn bộ danh sách người dùng. Và kiểm tra trên Postman.
        public function getAllUsers(){
            $this->apiOutput(json_encode($this->users));
        }
        //Phương thức getUserById($id): Gọi phương thức apiOutput() trả về thông tin của người dùng tại chỉ số $id trong mảng users.  Chạy thử trên Postman.
        public function getUserById($id){
            $this->apiOutput((json_encode($this->users[$id])));
        }
        // Phương thức addNewUser(): Lấy thông tin của người dùng gửi lên từ body request và thêm vào mảng users. Gọi phương thức apiOutput() để trả thông báo kết quả về cho người dùng. Chạy thử trên Postman.
        public function addNewUser(){
            // Lấy dữ liệu qua request body:
            $userData = stripslashes(file_get_contents('php://input'));
            // Hoán chuyển từ chuỗi nhận được sang dạng mảng
            $userData = json_decode($userData, true);
            // Gắn vào cuối mảng
            $this->users[] = $userData;
            // Chuyển $this->users về dạng chuỗi json và lưu vào file users.json
            file_put_contents('users.json', json_encode($this->users));
            $this->apiOutput(json_encode('{"message": "success", "newId": ' .count($this->users)-1 .'}'),"201");
        }
        // Phương thức updateUser(): Lấy thông tin của người dùng gửi lên từ client và cập nhật hồ sơ người dùng trong mảng users. Gọi phương thức apiOutput() để trả thông báo kết quả về cho người dùng. Chạy thử trên Postman.
        public function updateUser(){
            $id = $_GET["id"];
            $userData = stripslashes(file_get_contents('php://input'));
            $userData = json_decode($userData, true);
            $this->users[$id] = $userData;
            file_put_contents('users.json', json_encode($this->users));
            $this->apiOutput(json_encode('{"message": "success", "editedId": ' . $id .'}'),"200");
        }
        //Phương thức deleteUser($id): Xóa user tại chỉ số $id trong mảng $users. Gọi phương thức apiOutput() để trả thông báo kết quả về cho người dùng. Chạy thử trên Postman.
        public function deleteUser(){
            $id = $_GET["id"];
            // Hủy bỏ phần tử tại chỉ số $id, hàm uset là hủy 1 biến
            unset($this->users[$id]);
            file_put_contents('users.json', json_encode($this->users));
            $this->apiOutput(json_encode('{"message": "success", "user id deleted: ": ' . $id .'}'),"204");
        }
    }

    $users = new users();
    switch ($_SERVER["REQUEST_METHOD"]){
        // Xử lý request method GET
        case "GET": 
            if (isset($_GET["id"])){
                // trả về thông tin của 1 người dùng
                $users->getUserById($_GET["id"]);
            } else {
                $users->getAllUsers();
            }
            break;
        // xử lý request method POST
        case "POST":
            $users->addNewUser();
            break;
        // xử lý request method PUT
        case "PUT": 
            $users->updateUser();
            break;
        // xử lý request method Delete
        case "DELETE":
            $users->deleteUser();
            break;
    }
    $users->apiOutput("{'error':'Page not found'}", "404");
?>